<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {   
        if ($request->user()) {
            
             //echo $request->user()->role; exit;
            
            if ($request->user()->role ==1) {
                return redirect('dashboard1');
            }

            if ($request->user()->role ==2) {
                return redirect('dashboard2');
            }
        }
       
        
        return $next($request);
    }
}
