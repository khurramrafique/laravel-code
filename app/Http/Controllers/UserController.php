<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Validator;
use auth\Auth;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Cartalyst\Sentinel\Activations\EloquentActivation;



class UserController extends Controller
{
    /*
    public function addPermission()
    {
        $permission = Input::get('permission');

        $role = Sentinel::findRoleBySlug('admin');

        $role->addPermission($permission);
        $role->save();

        return Redirect::back()->withSuccess('Permission added successfully.');
    }
    */

    public function create_role(Request $request)
    {  
        $role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        echo "<pre>"; print_r($role); exit;
    }

    public function assign_role(Request $request)
    {  
        $user = Sentinel::findById(2);

        $role = Sentinel::findRoleByName('Admin');

        $role->users()->attach($user);
    }

    public function add_permissions()
    {
        $user = Sentinel::findById(1);
        $user->permissions = [
            'user.create' => true,
            'user.delete' => false,
        ];
        $user->save();
    }

    public function create(Request $request)
    {   
        if ($request->isMethod('post')) 
        {  
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required',
                'roles' => 'required',
                'email' => 'email|required'
            ]);

            if (!$validator->fails()) 
            {
                $credentials = [
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                ];
                $user = Sentinel::registerAndActivate($credentials);
                return redirect()->back()->with('status', 'User created successfully!');
            }
            else
            {
                return redirect('user/create')
                ->withErrors([
                    $validator->errors()->all()
                ]);    
            }
        }
        return response()->view('user.create');
    }
    
    public function UpdateUser(Request $request,$id)
    {   
        $User = User::find($id);
        if ($request->isMethod('post')) 
        { 
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'email|required'
            ]);

            if (!$validator->fails()) 
            {
            //$User = User::find($request->user()->id);
            $User->first_name = $request->input('first_name');
            $User->last_name = $request->input('last_name');
            $User->email = $request->input('email');
            $User->save();
            return redirect()->back()->with('status', 'Profile updated successfully!');
            }
            else
            {
                return redirect('profile')
                ->withErrors([
                    $validator->errors()->all()
                ]);    
            }
        }
        return response()->view('user.edit',$User);
    }

    public function UserListing()
    {   
        $users = User::all();
        return response()->view('user.list',['users'=>$users]);
    }

    public function ViewUser($id)
    {   
        $user = User::find($id);
        return response()->view('user.view',$user);
    }

    public function DeleteUser($id)
    {
        $user = Sentinel::findById($id);
        $user->delete();
        return redirect()->back()->with('status', 'User has been deleted successfully!');
    }
    
    public function SuspendUser($id)
    {   
        $user = Sentinel::findById($id); 
        Activation::remove($user);
        return redirect()->back()->with('status', 'User has been suspended successfully!');
    }

    public function getlogin()
    {
        return view('user.login');
    }

    public function postlogin(Request $request)
    {   echo $request->input('email'); exit;
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            // Authentication passed...
            //return redirect()->intended('dashboard');
            echo "Authentication passed"; exit;
        }
        echo "here"; exit;       
    }

    public function getlogout(Request $request)
    {
        Sentinel::logout(null, true);
        return redirect('users')->with('status', 'User has been logged out successfully!');
    }
}