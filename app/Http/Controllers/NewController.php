<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;
use App\Http\Requests;
use Validator;
use App\Comments;


class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {       
        //return "Hello Its a new Controller";
    }

    public function test()
    {      
        $comments = new Comments;
        $comments->place_id = 1;
        $comments->username = "user@username.com";
        $comments->comment = "Static Comments";

        $comments->save();

        $url = action('NewController@testrequest');
        return response()->view('new', ['url' => $url])->header('Content-Type', 'html');
    }

    public function testrequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required',
            'email' => 'email|required'
        ]);

        if ($validator->fails()) {
           echo "<pre>"; print_r($validator->errors()->all()); exit;
        }

        $name = $request->input('name');
        $uri = $request->path();
        $url = $request->url();
        $method = $request->method();
        echo $method; exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}