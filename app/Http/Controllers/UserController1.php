<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Users\UserInterface;

class UserController extends Controller
{
    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function Profile(Request $request)
    {
        if ($request->user()) {           
            return response()->view('auth.profile', $request->user());
        }
    }

    public function create_user(Request $request)
    {
        $credentials = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'email_verification' => $request->input('email_verification'),
            'verification_token' => md5(microtime().rand())
        ];

        $user = Sentinel::registerAndActivate($credentials);
    }

    public function check_user()
    {
        $user = Sentinel::findById(1);

        return Sentinel::login($user);
    }

    public function verify_email($id)
    {
        $user = User::where('verification_token', $id)->first();
        if($user)
        {
            $update_user = User::where('verification_token', $id)->update(['email_verification' => 1]);
            if($update_user)
            {   
                Auth::login($user);
                return redirect('profile');
            }
            else
            {
                return redirect('login')
                ->withErrors([
                'email' => 'Email Verification token has been expired',
            ]);
            }
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email|required'
        ]);

        if (!$validator->fails()) 
        {
        $User = User::find($request->user()->id);
        $User->first_name = $request->input('first_name');
        $User->last_name = $request->input('last_name');
        $User->email = $request->input('email');
        $User->save();
        return redirect()->back()->with('status', 'Profile updated successfully!');
        }
        else
        {
            return redirect('profile')
            ->withErrors([
                $validator->errors()->all()
            ]);    
        }
    }
}