<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', 'NewController@test');

Route::post('/request', 'NewController@testrequest');


// Users routes...
Route::any('user/create', 'UserController@create');

Route::get('users', 'UserController@UserListing');

Route::get('view/{id}', 'UserController@ViewUser');

Route::any('update/{id}', 'UserController@UpdateUser');

Route::get('delete/{id}', 'UserController@DeleteUser');

Route::get('suspend/{id}', 'UserController@SuspendUser');


// Pages routes...
Route::get('pages', 'PageController@index');

Route::get('page/create', 'PageController@create');

Route::post('page/store', 'PageController@store');

Route::get('page/view/{id}', 'PageController@show');

Route::get('page/edit/{id}', 'PageController@edit');

Route::post('page/update/{id}', 'PageController@update');

Route::get('page/delete/{id}', 'PageController@destroy');


Route::get('home', [
    'middleware' => 'role',
    'uses' => 'ProfileController@index'
]);

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('profile', [
    'middleware' => 'role',
    'uses' => 'ProfileController@index'
]);

Route::get('dashboard1', 'ProfileController@dashboard1');
Route::get('dashboard2', 'ProfileController@dashboard2');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);