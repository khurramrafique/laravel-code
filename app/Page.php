<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'nav_title', 'slug', 'body', 'css', 'js', 'show_title', 'show_in_nav'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    //protected $hidden = ['password', 'remember_token'];
}
