@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">View Page</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					
						<div class="form-group">
							<label class="col-md-4 control-label">Page Title</label>
							<div class="col-md-6">
								{{ $title }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Page Nav Title</label>
							<div class="col-md-6">
								{{ $nav_title }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Page Slug</label>
							<div class="col-md-6">
								{{ $slug }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Page Body</label>
							<div class="col-md-6">
								{{ $body }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Page CSS</label>
							<div class="col-md-6">
								{{ $css }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Page JS</label>
							<div class="col-md-6">
								{{ $js }}
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Show Title</label>
							<div class="col-md-6 ">
								@if ($show_title == 1)
								YES	
								@else
								NO
								@endif
							</div>
						</div>
						<br/>
						<div class="form-group">
							<label class="col-md-4 control-label">Show in Nav</label>
							<div class="col-md-6 ">
								@if ($show_in_nav == 1)
								YES	
								@else
								NO
								@endif
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
