@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Pages</div>
				<div class="panel-body">
				<style type="text/css">th,td{ padding: 20px;} </style>
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<table cellpadding="25">
						<tr >
							<th>Page Name</th><th>Page Slug</th><th>Options</th>
						</tr>
						@foreach ($pages as $page)
							<tr><td>{{ $page->title }}</td> <td>{{ $page->slug }}</td>
							<td>
								<a href="{{ url('page/view/'.$page->id) }}">View</a>|
								<a href="{{ url('page/edit/'.$page->id) }}">Edit</a>|
								<a href="{{ url('page/delete/'.$page->id) }}">Delete</a>
							</td>
						</tr>
						@endforeach		
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
